#!/bin/bash
mkdir -p profiles
mkdir -p examples
mkdir -p valuesets
for file in BSeR*.xml; do
    mv -i $file ./profiles/${file#BSeR}
done
for file in *example*.xml; do
    mv -i $file ./examples/${file}
done
for file in *-*.xml; do
    mv -i $file ./valuesets/${file}
done