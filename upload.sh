#!/bin/bash
while getopts ":s:" opt; do
  case $opt in
    s)
      SERVER=$OPTARG
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done
if [ -z ${SERVER} ]
   then
      echo "No Server Specified"
      exit
fi
for filename in valuesets/*.xml; do
    echo "PUT Valueset filename :" ${filename}
    BASEPATH=$(basename "$filename")
    FILE="${BASEPATH%.*}"
    curl -X PUT -H "Content-Type: application/xml" ${SERVER}/ValueSet/${FILE} --upload-file ${filename}
done
for filename in profiles/*.xml; do
    echo "PUT StructureDefinition filename :" ${filename}
    BASEPATH=$(basename "$filename")
    FILE="${BASEPATH%.*}"
    curl -X PUT -H "Content-Type: application/xml" ${SERVER}/StructureDefinition/${FILE} --upload-file ${filename}
done
exit
