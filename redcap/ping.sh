#!/bin/sh
DATA="token=B2FC1E4EFAC6E24741809FA788BCAE40&content=record&format=json&type=flat&overwriteBehavior=normal&forceAutoNumber=false&returnContent=count&returnFormat=json&data=[{\"record_id\":\"5\",\"firstname\":\"Megan\",\"lastname\":\"Light\",\"age\":\"16\"}]"
CURL=`which curl`
$CURL -H "Content-Type: application/x-www-form-urlencoded" \
      -H "Accept: application/json" \
      -X POST \
      -d $DATA \
      https://redcap-azure-stage.ymca.net/api/