package gov.cdc.ncpdd.bserfhir;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.Provider;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.hl7.fhir.dstu3.model.Address;
import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.dstu3.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Coding;
import org.hl7.fhir.dstu3.model.Composition;
import org.hl7.fhir.dstu3.model.ContactPoint;
import org.hl7.fhir.dstu3.model.MessageHeader;
import org.hl7.fhir.dstu3.model.Narrative;
import org.hl7.fhir.dstu3.model.Observation;
import org.hl7.fhir.dstu3.model.Organization;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Practitioner;
import org.hl7.fhir.dstu3.model.PractitionerRole;
import org.hl7.fhir.dstu3.model.Quantity;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.ReferralRequest;
import org.hl7.fhir.dstu3.model.Resource;
import org.hl7.fhir.dstu3.model.ResourceType;
import org.hl7.fhir.dstu3.model.codesystems.AdministrativeGender;
import org.hl7.fhir.utilities.xhtml.NodeType;
import org.hl7.fhir.utilities.xhtml.XhtmlNode;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.context.PerformanceOptionsEnum;
import ca.uhn.fhir.model.api.IResource;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.api.ServerValidationModeEnum;


public class App 
{
    public static void main( String[] args )
    {
    	try {
    			prepareFeedBackBundle();
    	
    	}catch(Exception e) {
    		e.printStackTrace();
    	
    	}
    	
    	/*FhirContext ctx = FhirContext.forDstu3();
    	ctx.getRestfulClientFactory().setServerValidationMode(ServerValidationModeEnum.NEVER);
    	ctx.setPerformanceOptions(PerformanceOptionsEnum.DEFERRED_MODEL_SCANNING);
    	ctx.getRestfulClientFactory().setConnectTimeout(20 * 1000);
    	
    	String serverBase = "https://eip-fhir.experimental.aimsplatform.com/hapi-fhir/baseDstu3";
    	IGenericClient client = ctx.newRestfulGenericClient(serverBase);
    	
    	
    	Bundle results = client
    		      .read()
    		      .resource(Bundle.class)
    		      //.withIdAndVersion("43", "2")
    		     .withId("44")
    		      .execute();
    	
    	HashMap<String,String> map = processReferral(results);
    	
    	for (String key : map.keySet()) {
    		String value = map.get(key);
			System.out.print(key+"="+value+"\n");
		}*/
//    	PractitionerRole pr =  new PractitionerRole();
//    	//pr.addCode(new COdable Coding("", "", ""));
//    	
//    	MethodOutcome outcome = client
//    		.create()
//    		.resource(getPatient())
//    		.execute();
    	
    	
        //System.out.println( "Hello World!"+Bundle.IDENTIFIER );
    }
    public static Map<String,String>getTestMap(){
    	
    	HashMap<String , String> map = new HashMap<String , String>();
    	//Patient variables
    	map.put("patient_mrn_id","123Test"); 	
		map.put("patient_given_name","JarJar");
		map.put("patient_name_prefix","Mr");
		map.put("patient_dob","1951-10-10");
		
		
		//Composition
		map.put("feedback_referral_id", "FeedbacktestID123");
		map.put("feedback_note", "This is note coming from the redcap create feedback form");
		
		
		//support bundle
		map.put("bmi", "120");
		map.put("body_weight", "120");
		map.put("body_height", "120");
		map.put("a1c_count", "120");
		
    	return map;
    }
    
    public static void prepareFeedBackBundle() throws Exception{
    	FhirContext ctx = FhirContext.forDstu3();
    	ctx.getRestfulClientFactory().setServerValidationMode(ServerValidationModeEnum.NEVER);
    	ctx.setPerformanceOptions(PerformanceOptionsEnum.DEFERRED_MODEL_SCANNING);
    	ctx.getRestfulClientFactory().setConnectTimeout(20 * 1000);
    	
    	String serverBase = "https://eip-fhir.experimental.aimsplatform.com/hapi-fhir/baseDstu3";
    	
    	String feedBackStringJSON = readFilesAsString("/Feedback-A.xml");
    	
    	Bundle feedback = (Bundle) ctx.newXmlParser().parseResource(feedBackStringJSON);
    	
    	System.out.println( "Feedback"+feedback.fhirType());
    	
    	List<Resource> list = processFeedBackBundle(feedback,getTestMap());
    	
    	
    	
    }
    public static List<Resource> processFeedBackBundle(Bundle bundle,Map map) {
        //generate a list of resources...
        List<Resource> theResources = new ArrayList<Resource>();    //list of resources in bundle
        for (BundleEntryComponent entry : bundle.getEntry()) {
            theResources.add(entry.getResource());
        }
        return process(theResources,map);
    }
    
    
    private static Patient getPatient(List<Resource> theResources,Map<String,String> map) {
    	
    	for(Resource resource : theResources) {
     	   System.out.println(resource.getResourceType());
     	   if(ResourceType.Patient.equals(resource.getResourceType())) {
     		   Patient p = (Patient)resource;
     		   return p;
     	   }
    	}
    	return null;
    }
    
    
    private static List<Resource> process(List<Resource> theResources,Map<String,String> map) {
        Patient patient = getPatient(theResources,map);     //this will be the patient resource. There should only be one....
        List<Resource> processed = new ArrayList<Resource>();
        
       for(Resource resource : theResources) {
    	   System.out.println(resource.getResourceType());
    	   if(ResourceType.Patient.equals(resource.getResourceType())) {
    		   Patient p = (Patient)resource;
    		   System.out.println(p.getIdentifierFirstRep().getValue());
    		   patient= p;
       		   processed.add(processPatient(p,map));
    	   }else if(ResourceType.MessageHeader.equals(resource.getResourceType())) {
    		   //NOOP Just use the same from the xml
    		   
    	   }else if(ResourceType.Composition.equals(resource.getResourceType())){
    		   Composition c = (Composition) resource;
    		   c.getIdentifier().setValue(map.get("feedback_referral_id"));
    		   Narrative n = new Narrative();
    		   n.setDivAsString(map.get("feedback_note"));
    		   c.getSectionFirstRep().setText(n);
    		   c.setSubject(new Reference(patient.getId()));
    		   processed.add(c);
    	   }else if(ResourceType.Observation.equals(resource.getResourceType())){
    		   //Education Level 
    	   }else if(ResourceType.Bundle.equals(resource.getResourceType())){
    		   Bundle bundle = (Bundle) resource;
    		   for (BundleEntryComponent entry : bundle.getEntry()) {
    	           Observation o = (Observation)entry.getResource();
    	           if("39156-5".equals(o.getCode().getCodingFirstRep().getCode())){
    	        	   //BMI
    	        	   o.setValue(new Quantity(Long.parseLong(map.get("bmi"))));
    	        	   
    	        	   
    	           }else if("29463-7".equals(o.getCode().getCodingFirstRep().getCode())){
    	        	   //body weight
    	        	   
    	        	   o.setValue(new Quantity(Long.parseLong(map.get("body_weight"))));
    	        	   
    	           }else if("8302-2".equals(o.getCode().getCodingFirstRep().getCode())){
    	        	   //body height
    	        	   
    	        	   o.setValue(new Quantity(Long.parseLong(map.get("body_height"))));
    	        	   
    	           }else if("4548-4".equals(o.getCode().getCodingFirstRep().getCode())){
    	        	   //a1c count
    	        	   
    	        	   o.setValue(new Quantity(Long.parseLong(map.get("a1c_count"))));
    	        	   
    	           }
    	           //set the patient link
    	           o.setSubject(new Reference(patient.getId()));
    	           System.out.println(o.getValueQuantity().getValue());
    	        }
    		   processed.add(bundle);
    	   }
       }
 
       return theResources;
    }
    
    public static Patient processPatient(Patient p ,Map<String,String> map) {
    	
    	p.getIdentifierFirstRep().setValue(map.get("patient_mrn_id"));
    	p.getNameFirstRep()
    			.setFamily(map.get("patient_family_name"))
    			.addGiven(map.get("patient_given_name"))
    			.addPrefix(map.get("patient_name_prefix"));
    	p.setBirthDate(Date.valueOf(map.get("patient_dob")));
    	return p;
    }
    
    public static String readFilesAsString(String filePath) throws Exception{	
    	
    	InputStream inputStream = App.class.getResourceAsStream(filePath);
		return IOUtils.toString(inputStream);		
	}
    
    
    private static HashMap<String,String> processReferral(Bundle results){
    	HashMap<String,String> map = new LinkedHashMap<String, String>();
    	List<BundleEntryComponent> entries = results.getEntry();
    	for (BundleEntryComponent bundleEntryComponent : entries) {
			Resource resource = bundleEntryComponent.getResource();
			System.out.print(resource.getResourceType().name()+"\n");
			if((ResourceType.MessageHeader).name().equalsIgnoreCase((resource.getResourceType().name()))) {
				MessageHeader mheader = (MessageHeader)resource;
				Practitioner receiver = (Practitioner) mheader.getReceiver().getResource();
				Practitioner sender = (Practitioner) mheader.getSender().getResource();
				map.put("reason", mheader.getReason().getCodingFirstRep().getDisplay());
				
				map.put("sender_address", getAddress(sender.getAddressFirstRep()));
				map.put("receiver_address", getAddress(receiver.getAddressFirstRep()));
				map.put("sender_practitioner", sender.getNameFirstRep().getNameAsSingleString());
				map.put("receiver_practitioner_name", receiver.getNameFirstRep().getNameAsSingleString());
				//map.put("sender_organization", value)
				//map.put("receiver_organization",value)
				
			}else if((ResourceType.ReferralRequest).name().equalsIgnoreCase((resource.getResourceType().name()))) {
				ReferralRequest referral = (ReferralRequest)resource;
				map.put("referral_reason", referral.getServiceRequestedFirstRep().getCodingFirstRep().getDisplay());
				map.put("referral_identifier", referral.getIdentifierFirstRep().getValue());
				//Referral Intent and Referral Status
				//map.put("referral_intent", referral.getIntent().getDisplay());
				//map.put("referral_intent", referral.getStatus().getDisplay());
				
				
			}
		} 
    	return map;
    }
    
    private static String getAddress(Address address) {
    	String str = "";
    	str  = str + (address.getLine()).get(0);
    	str = str + ","+ address.getCity();
    	str = str + ","+ address.getState();
    	str = str + "," + address.getPostalCode();
    	return str;
    }
    
    
    
    private static Patient getPatient() {
    	
    	Patient patient = new Patient();
    	
    	patient.addName().addGiven("John").setFamily("Smith");
    	patient.getBirthDateElement().setValueAsString("1998-02-22");
    	patient.addIdentifier().setType(new CodeableConcept().addCoding(new Coding("http://hl7.org/fhir/ValueSet/identifier-type", "DL", "Drivers Licence")));
    	
    	
    	return patient;
    	
    	
    	
    }
    
    
    
    
    private static Practitioner getPractitioner1() {
    	
    	Practitioner practitioner = new Practitioner();
    	practitioner.addIdentifier().setValue("PractionerID1");
    	practitioner.addAddress(new Address()
    					.addLine("18640 West Belvidere Road")
    					.setCity("GreylLake")
    					.setDistrict("IL")
    					.setPostalCode("60030"));				
    	practitioner.addName().setFamily("Heard").addGiven("Elisa").addPrefix("Dr");
    	practitioner.addTelecom().setValue("(618) 942-2002");
    	
    	practitioner.addTelecom(new ContactPoint()
				.setValue("(618) 942-2002")
				.setSystem(ContactPoint.ContactPointSystem.PHONE)
				
			);
    	
    	return practitioner;
    	
    }
    
    
    private static Practitioner getPractitioner2() {
    	
    	Practitioner practitioner = new Practitioner();
    	practitioner.addIdentifier().setValue("PractionerID1");
    	practitioner.addAddress(new Address()
    					.addLine("18640 West Belvidere Road")
    					.setCity("Chicago")
    					.setDistrict("IL")
    					.setPostalCode("60251"));				
    	practitioner.addName().setFamily("Adam").addGiven("West").addPrefix("Dr");
    	
    	return practitioner;
    	
    }
    
    private static PractitionerRole getInitiator() {
    	
    	PractitionerRole practionerRole = new PractitionerRole();
   
    	practionerRole.setPractitioner(new Reference(getPractitioner1()));
      	practionerRole.setOrganization(new Reference(getOrganization1()));
    	
      	return practionerRole;
    	
    }
    
    private static MessageHeader createMessageHeader() {
    	
    	MessageHeader mheader = new MessageHeader();
    	//mheader.setSender(new Reference(.getRecipient()));
    	return mheader;
    	
    }
    
    
    
	private static PractitionerRole getRecipient() {
    	
    	PractitionerRole practionerRole = new PractitionerRole();
   
    	practionerRole.setPractitioner(new Reference(getPractitioner2()));
      	practionerRole.setOrganization(new Reference(getOrganization2()));
    	
      	return practionerRole;
    	
    }
    
    
    private static Organization getOrganization1() {
    	Organization organization = new Organization();
    	organization
    			.addIdentifier()
    			.setSystem("urn:oid:2.16.840.1.113883.4.6")
    			.setValue("1659603009")
    			.setType(new CodeableConcept()
		    						.addCoding(new Coding()
				    						.setCode("prov")
				    						.setDisplay("Healthcare Provider")
				    						.setSystem("http://hl7.org/fhir/ValueSet/organization-type")
    			));
    	organization.setName("Alliance Of Chicago Therapeutic Services and Supplies");
    	organization.addAddress(new Address()
			.addLine("18640 West Belvidere Road")
			.setCity("Chicago")
			.setDistrict("IL")
			.setPostalCode("60251"));	
    	
    	organization.addTelecom(new ContactPoint()
    								.setValue("(773)-661-4499")
    								.setSystem(ContactPoint.ContactPointSystem.PHONE)
    								
    							);
    	
    	return organization;
  
    }
    
    
    private static Organization getOrganization2() {
    	Organization organization = new Organization();
    	organization
    			.addIdentifier()
    			.setSystem("urn:oid:2.16.840.1.113883.4.6")
    			.setValue("1215372123")
    			.setType(new CodeableConcept()
		    						.addCoding(new Coding()
				    						.setCode("prov")
				    						.setDisplay("Healthcare Provider")
				    						.setSystem("http://hl7.org/fhir/ValueSet/organization-type")
    			));
    	organization.setName("YMCA of Metropolitan Chicago");
    	organization.addAddress(new Address()
			.addLine("1030 W Van Buren St")
			.setCity("Chicago")
			.setDistrict("IL")
			.setPostalCode("60607"));	
    	
    	organization.addTelecom(new ContactPoint()
    								.setValue("(909) 427-5000")
    								.setSystem(ContactPoint.ContactPointSystem.PHONE)
    								
    							);    	
    	return organization;
  
    }
    
    
    
    private static void getSupportBundle() {
    	
    	Bundle obesitySupportBundle = new Bundle();
    	
    }
    
//    private static Observation getBodyHeightObservation() {
//    	
//    	CodeableConcept concept = new CodeableConcept()
//    									.addCoding(new Coding("http://loinc.org","80913-7","Highest level of education")
//    									.setCode("")
//    									.set);
//    	Observation observation = new Observation();
//    	observation.setStatus( org.hl7.fhir.dstu3.model.Observation.ObservationStatus.FINAL);
//    	observation.setCode(concept);
//    	observation.setSubject(new Reference(getPatient()));
//    	observation.setValue(concept);
//    	
//    	return observation;
//    }
    
    
    
}
